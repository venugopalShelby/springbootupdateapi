package com.productapi.userupdate.service;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.productapi.userupdate.entity.Users;
import com.productapi.userupdate.repository.UserRepository;

@SpringBootTest
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    //success when  valid user object and a valid user ID are provided
    @Test
    public void testUpdateUser_Success() {
        Users user = new Users();
        user.setId(1);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("johndoe@example.com");
        user.setPhoneNo("1234567890");

        when(userRepository.existsById(1)).thenReturn(true);
        when(userRepository.save(any(Users.class))).thenReturn(user);

        Users updatedUser = userService.updateUser(user, 1);

        assertEquals(user, updatedUser);
    }

    //User with id 1 is not found in the db
    @Test
    public void testUpdateUser_UserNotFound() {
        Users user = new Users();
        user.setId(1);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("johndoe@example.com");
        user.setPhoneNo("1234567890");

        when(userRepository.existsById(1)).thenReturn(false);

        assertThrows(IllegalArgumentException.class, () -> {
            userService.updateUser(user, 1);
        });
    }


    //testing the scenario where the user object passed to the updateUser() method is empty,
    
    @Test
    public void testUpdateUser_EmptyUser() {
        Users user = new Users();
        user.setId(1);
        
        when(userRepository.existsById(1)).thenReturn(true);
        when(userRepository.save(any(Users.class))).thenReturn(user);
        
        Users updatedUser = userService.updateUser(user, 1);

        assertEquals(user, updatedUser);
    }
    
    
    //testing the scenario where the user object passed to the updateUser() method does not have an ID field set.
    @Test
    public void testUpdateUser_UserWithoutId() {
        Users user = new Users();
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("johndoe@example.com");
        user.setPhoneNo("1234567890");
        
        assertThrows(IllegalArgumentException.class, () -> {
            userService.updateUser(user, 1);
        });
    }
    

}
