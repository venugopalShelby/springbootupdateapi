package com.productapi.userupdate.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.productapi.userupdate.entity.Users;
import com.productapi.userupdate.service.UserService;






@SpringBootTest
public class UserControllerTest {

    @Autowired
    private UserController userController;

    @MockBean
    private UserService userService;

    
    //tests if the method correctly updates a user by verifying that the HTTP response code is 200 (OK) and 
    //the response body is the updated user object.
    @Test
    public void testUpdateUser() throws Exception {
        Users user = new Users();
        user.setId(1);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("john.doe@example.com");
        user.setPhoneNo("555-555-1234");

        when(userService.updateUser(user, 1)).thenReturn(user);

        ResponseEntity<Object> responseEntity = userController.updateUser(user, 1);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());
    }

    //test is checking the behavior when the user with the given ID is not found in the system.
    @Test
    public void testUpdateUserNotFound() throws Exception {
        Users user = new Users();
        user.setId(1);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("john.doe@example.com");
        user.setPhoneNo("555-555-1234");

        when(userService.updateUser(user, 1)).thenThrow(new IllegalArgumentException("User with ID 1 does not exist."));

        ResponseEntity<Object> responseEntity = userController.updateUser(user, 1);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("User with ID 1 does not exist.", responseEntity.getBody());
    }
}


