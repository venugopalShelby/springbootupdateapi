package com.productapi.userupdate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.productapi.userupdate.entity.Users;
import com.productapi.userupdate.repository.UserRepository;
import com.productapi.userupdate.service.UserService;



@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	
	@Autowired
	private UserService userService;
	
	@PutMapping(value="/update/{id}")
	@CrossOrigin
	public ResponseEntity<Object> updateUser(@RequestBody Users user, @PathVariable("id") Integer id) {
		try {
			Users updatedUser = userService.updateUser(user, id);
			return ResponseEntity.ok().body(updatedUser);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}	
	
}
