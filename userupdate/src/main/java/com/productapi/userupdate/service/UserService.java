package com.productapi.userupdate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.productapi.userupdate.entity.Users;
import com.productapi.userupdate.repository.UserRepository;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Users updateUser(Users user, Integer id) {
        if (!userRepository.existsById(id)) {
            throw new IllegalArgumentException("User with ID " + id + " does not exist.");
        }
        return userRepository.save(user);
    }

   
}
