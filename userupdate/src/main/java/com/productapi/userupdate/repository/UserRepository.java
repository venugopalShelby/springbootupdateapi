package com.productapi.userupdate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.productapi.userupdate.entity.Users;

@Repository
public interface UserRepository extends JpaRepository <Users, Integer> {

}
